FROM golang:alpine
RUN apk update && \
    apk upgrade && \
    apk add git
RUN mkdir /app
RUN cd /app && \
    git clone https://github.com/adobe/prometheus-emcisilon-exporter.git && \
    cd prometheus-emcisilon-exporter && \
    go get ./... ; \
    go build

EXPOSE 9300

CMD /app/prometheus-emcisilon-exporter/prometheus-emcisilon-exporter \
  --isilon.cluster.fqdn ${ISILON_HOSTNAME} \
  --isilon.cluster.username ${ISILON_CLUSTER_USERNAME} \
  --no-collector.node_info \
  --no-collector.quota \
  --no-collector.nfs_exports \
  --no-collector.smb_shares \
  --no-collector.sync_iq \
  --no-collector.node_partition \
  --no-collector.storage_pools \
  --no-collector.snapshots \
  --no-collector.quota_summary \
  --no-collector.protocol_common.siq
